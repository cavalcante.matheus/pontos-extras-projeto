#!/bin/bash

gerar_senha() {
    local tamanho=$1
    local caracteres=$2
    local senha=""
    
    for (( i=0; i<$tamanho; i++ )); do
        senha+=${caracteres:RANDOM%${#caracteres}:1}
    done
    
    echo "$senha"
}

dialogo_senha() {
    local tamanho_senha=$(yad --entry --title "Gerador de Senhas" --text "Digite o tamanho da senha:" --entry-text "8")
    
    local caracteres_senha=$(yad --entry --title "Gerador de Senhas" --text "Digite os caracteres permitidos na senha:")
    
    if [[ ! -z $caracteres_senha ]]; then
        local senha1=$(gerar_senha "$tamanho_senha" "$caracteres_senha")
        local senha2=$(gerar_senha "$tamanho_senha" "$caracteres_senha")
        local senha3=$(gerar_senha "$tamanho_senha" "$caracteres_senha")
        
        yad --info --title "Senhas Geradas" --text "Senha 1: $senha1\nSenha 2: $senha2\nSenha 3: $senha3"
    else
        yad --info --title "Senhas Geradas" --text "Nenhum caractere foi inserido."
    fi
}

dialogo_senha
